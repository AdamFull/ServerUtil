from os import chdir, devnull
from subprocess import Popen, STDOUT
from os.path import isfile
from threading import Thread
import configparser

ram_count = 4
server_folder = "ServerName"
executable = "spigot.jar"
config = configparser.SafeConfigParser()
server_thread = None

def FakeSecHead(fp): yield '[ServerProps]\n'; yield from fp

if __name__ == "__main__":
    chdir(server_folder)
    
    if not isfile("eula.txt"):
        with open("eula.txt", "w") as f:
            f.write("eula=True")

    server_thread = Popen("java -Xms{0}G -Xmx{0}G -XX:+UseConcMarkSweepGC -jar {1} nogui java".format(ram_count, executable), stdout=open(devnull, 'w'))

    #server_thread.terminate()

    config.readfp(FakeSecHead(open('server.properties')))
    print(config.items("ServerProps"))

    while(True):
        server_thread.communicate(input("Server command: "), 10)