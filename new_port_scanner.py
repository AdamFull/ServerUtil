# This script runs on Python 3
import socket, threading
from datetime import datetime

def TCP_connect(ip, delay, start, end):
    TCPsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    TCPsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    TCPsock.settimeout(delay)
    for port_number in range(start, end):
        try:
            TCPsock.connect((ip, port_number))
            print("Port %s: listening" % port_number)
        except Exception as e:
            continue

# Поделить по кучкам на потоки( 51 поток ко 1285)

def scan_ports(host_ip, delay, threead_amount):
    threads = []        # To run TCP_connect concurrently
    output = {}         # For printing purposes
    current_port_number = 0

    print("Start scanning...")
    t1 = datetime.now()

    # Spawning threads to scan ports
    for i in range(threead_amount-1):
        t = threading.Thread(target=TCP_connect, args=(host_ip, delay, int(current_port_number), int((i+1)*(65535/threead_amount))))
        current_port_number = int((i+1)*(65535/threead_amount))
        threads.append(t)
        threads[i].start()

    # Locking the main thread until all threads complete
    for thread in threads:
        thread.join()

    print("Scanning time: %s, active threads: %s" % (datetime.now()-t1, threading.activeCount()))



def main():
    host_ip = socket.gethostbyname(socket.gethostname())
    print("Scanning the %s" % host_ip)
    scan_ports(host_ip, 0.1, 51)

if __name__ == "__main__":
    main()