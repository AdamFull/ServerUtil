from os import system, popen
import platform

class PortScanner:
    def __init__(self):
        self.os = platform.system()
        self.netstat_data = []

        if self.os == "Windows":
            self.data_filter(popen("netstat -an|find /i \"listening\"").read())
        elif self.os == "Linux":
            self.data_filter(popen("netstat -tulpn | grep LISTEN").read())
        elif self.os == "Darwin":
            self.data_filter(popen("netstat -pant | grep LISTEN").read())
    
    def __repr__(self):
        return repr(self.netstat_data)
    
    def del_from_index(self, ur_list, index):
        if isinstance(index, list):
            result = ur_list
            for ind in index:
                result = result[:ind] + result[ind+1 :]
            return result
        else:
            return ur_list[:index] + ur_list[index+1 :]
    
    def data_filter(self, netstat_result):
        netstat_data, temp_data = [], []
        netstat_filtred = [x for x in netstat_result.split(" ") if not x == ""]
        for string in netstat_filtred:
            temp_data.append(string.replace("\n", ""))
            if self.os == "Windows":
                if string == "LISTENING\n":
                    netstat_data.append(self.del_from_index(temp_data, 2))
                    temp_data = []
            elif self.os == "Linux":
                if "/" in string or "-" in string:
                    netstat_data.append(self.del_from_index(temp_data, [1,1,2,3]))
                    temp_data = []
            elif self.os == "Darwin":
                if "\n" in string:
                    netstat_data.append(self.del_from_index(temp_data, [1,1,2]))
                    temp_data = []
        self.netstat_data = netstat_data

print(PortScanner())