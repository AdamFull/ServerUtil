# This script runs on Python 3
import socket, threading
from datetime import datetime

def TCP_connect(ip, port_number, delay, output, time):
    current_thread = threading.currentThread()
    TCPsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    TCPsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    TCPsock.settimeout(delay)
    try:
        TCPsock.connect((ip, port_number))
        print("Port %s: listening, time: %s" % (port_number, datetime.now()-time))
    except:
        return

# Поделить по кучкам на потоки( 51 поток ко 1285)

def scan_ports(host_ip, delay):
    threads = []        # To run TCP_connect concurrently
    output = {}         # For printing purposes

    print("Start scanning...")
    t1 = datetime.now()

    # Spawning threads to scan ports
    for i in range(65535):
        t = threading.Thread(target=TCP_connect, args=(host_ip, i, delay, output, t1))
        threads.append(t)
        threads[i].start()

    # Locking the main thread until all threads complete
    for thread in threads:
        thread.join()

    print("Scanning time: %s, active threads: %s" % (datetime.now()-t1, threading.activeCount()))



def main():
    host_ip = socket.gethostbyname(socket.gethostname())
    print("Scanning the %s" % host_ip)
    delay = 0.0001
    scan_ports(host_ip, delay)

if __name__ == "__main__":
    main()